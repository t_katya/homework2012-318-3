﻿#include"home_work_3.h"

//------------------------------------------------------------------

void reading_file ( void ) {

	int i = 0, k;
	fstream f1, f2, f3;
	ver_delay = NULL;

	f1.open( "graph.nodes", fstream::in );
	
	while( !f1.eof() ) {
		ver_delay = ( double * ) realloc ( ver_delay, ( ++i )*sizeof( double ) );
		f1 >> ver_delay[i-1];
	}
	N = i;
	f1.close();
	graph = new double *[N];
	
	for ( i = 0; i < N; ++i ) 
		graph[i] = new double[N]; 

	for ( i = 0; i < N; ++i ) {
	    for ( k = 0; k < N; ++k ) 
			graph[i][k] = -1.0;
	}
	f2.open( "graph.nets", fstream::in );
	
	while ( !f2.eof() ) {
		f2 >> i;
		f2 >> k;
		f2 >> graph[i][k];
	}
	f2.close();
	f3.open("graph.aux", fstream::in );

	RAT = new double[N];
	while ( !f3.eof() ) {
		f3 >> k;
	    f3 >> RAT[k];
	}
	f3.close();
	return;
}

//------------------------------------------------------------------

void AAT_count ( void ) {
    
	int i, v, u, k;
	int flg;
	int *temp = NULL;                         // -1 = white, 0 = gray, 1 = black 
	double max;
	queue <int> Q;

	AAT = new double [N];
	temp = new int [N];
	
	for ( i = 0; i < N; ++i ) temp[i] = -1; 
	
	for ( v = 0; v < N; ++v ) {
		flg = 0;
		for ( i = 0; i < N; ++i ) {
			if ( graph[i][v] != -1.0 ) { 
			    flg = 1; 
			}
		}
		if ( flg == 0 ) {
		    temp[v] = 0;
	        Q.push(v);	
        }
    }

    while ( !Q.empty() ) {		
		u = Q.front(); 
		Q.pop();
		AAT[u] = ver_delay[u];
		flg = 1;
		i = 0;
		max = -1.0;
		for ( k = 0; k < N; ++k ) {
			if ( graph[k][u] != -1.0 ) { 
				++i;
				if ( temp[k] != 1 )  {
		    	    Q.push(k);
					temp[k] = 0;
				    flg = 0;
			    }
		    }
		}
		if ( flg == 0 ) Q.push(u); 
		if ( ( flg == 1 ) && ( i > 0 ) ) {
			for ( k = 0; k < N; ++k ) {
				if ( ( graph[k][u] != -1.0 ) ) {
					if ( max < ( AAT[k] + graph[k][u] ) )
					    max = AAT[k] + graph[k][u];
				}
			}
			AAT[u] = max + AAT[u];
		}

	    for ( k = 0; k < N; ++k ) {
	        if ( graph[u][k] != -1.0 ) {
		        if ( temp[k] == -1 ) {
				    temp[k] = 0;
				    Q.push(k);
			    } 
	        }
	    }
		if ( flg != 0) temp[u] = 1;
	}
	return;
}

//------------------------------------------------------------------

void RAT_count ( void ) {
	int i, v, u, k;
	int flg;
	int *temp = NULL;                         // -1 = white, 0 = gray, 1 = black 
	double max;
	queue <int> Q;

	temp = new int [N];
	
	for ( i = 0; i < N; ++i ) temp[i] = -1; 
	
	for ( v = 0; v < N; ++v ) {
		flg = 1;
		for ( i = 0; i < N; ++i ) {
			if ( graph[v][i] != -1.0 ) { 
				flg = 0;
			}
		}
		if ( flg == 1 ) {
			temp[v] = 1;
		    for ( k = 0; k < N; ++k ) {
		        if ( graph[k][v] != -1.0 ) {
			        temp[k] = 0;
				    Q.push(k);
			    }
		    }
		}
    }

    while ( !Q.empty() ) {
		
		u = Q.front(); 
		Q.pop();
		flg = 1;
		i = 0;

		for ( k = 0; k < N; ++k ) {
			if ( graph[u][k] != -1 ) {
				if ( temp[k] != 1 ) {
					temp[k] = 0;
					Q.push(k);
					flg = 0;
				}
			}
		}
		if ( flg == 0 ) Q.push(u);
		else if ( flg == 1 ) {
			for ( k = 0; k < N; ++k ) {
			    if ( graph[u][k] != -1 ) {
					if ( i == 0 ) max = RAT[k] - graph[u][k];
			        ++i;
					if ( max < ( RAT[k] - graph[u][k] ) ) max = RAT[k] - graph[u][k];
			    }
			}
				RAT[u] = max - ver_delay[u];
		}
		for ( k = 0; k < N; ++k ) {
		    if ( graph[k][u] != -1.0 ) {
				Q.push(k);
				temp[k] = 0;
			}
	    }
		if ( flg != 0 ) temp[u] = 1;
	}
	return;
}

//------------------------------------------------------------------

void slack_count ( void ) {
	int i;
	slack = new double [N];
	for ( i = 0; i < N; ++i )
        slack[i] = RAT[i] - AAT[i];
}

//-------------------------------------------------------------------

void filing ( void ) {

	int i, k = 0;
	fstream f1, f2;
	f1.open( "graph.slacks", fstream::out );
	for ( i = 0; i < N; ++i )
	    f1 << slack[i] << endl;
	f1.close();
	for ( i = 0; i < N; ++i ) {
	    if ( slack[i] < 0.0 ) ++k;
	}
	f2.open( "graph.result", fstream::out );
	    f2 << k << endl;
		if ( k > 0 ) {
			for ( i = 0; i < N; ++i ) {
				if ( slack[i] < 0.0 ) f2 << i << " ";
			}
		}
	f2.close();
	return;
}

//--------------------------------------------------------------------

int main () {
	
	reading_file (); 
    AAT_count ();
	RAT_count ();
	slack_count ();
    filing ();
	return 0;
}