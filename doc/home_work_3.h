﻿
/**
*Эта программа вычисляет для каждой вершины графа рассогласованность
*в вершине и находит все вершины с отрицательным значением
*@author Taratuta Yekaterna 
*/

#ifndef HOMEWORK_H
#define HOMEWORK_H

#include<iostream>
#include<fstream>
#include<cstdlib>
#include<cstdio>
#include<Queue>

using namespace std;
/**
*<bb>N</bb> - колличество вершин в графе. Инициализируется при считывании
*из файла <tt>graph.nodes</tt> в функции {@link reading_file}
*/
int N; 
/**
*<bb>ver_delay</bb> - массив задержек вершин в графе. Инициализируется при считывании
*из файла <tt>graph.nodes</tt> в функции {@link reading_file}
*/
double *ver_delay;
/**
*<bb>graph</bb> - матрица смежности. В ней информация о путях и задержках вершин в графе. 
*Инициализируется при считывании из файла <tt>graph.nets</tt> в функции {@link reading_file}
*/
double **graph;
/**
*<bb>AAT</bb> - маcсив. В нем информация о времени прихода нового сигнала. Инициализируется 
*в функции {@link AAT_count}
*/
double *AAT;
/**
*<bb>RAT</bb> - маcсив. В нем информация о времени, когда сигнал понадобится для вычислений. 
*Инициализация: выходные вершины инициализируются  при считывании из файла <tt>graph.aux</tt> 
*в функции {@link reading_file}, а остальные в функции {@link RAT_count}
*/
double *RAT;
/**
*<bb>slack</bb> - маcсив. В нем информация о рассошлассованности в вершинах. 
*Вычисляется в функции {@link slack_count}
*/
double *slack;

/**
*Эта функция считывает из файла <tt>graph.nodes</tt> информацию о задержках
*в каждой вершине. Из файла <tt>graph.nets</tt> информацию о задержках 
*каждого ребра. Из файла <tt>graph.aux</tt> считывается информация о 
*значении RAT в выходных вершинах.
*@author Taratuta Yekaterina
*@see filing
*/
void reading_file (void);
/**
*Эта функция записывает в файл <tt>graph.slacks</tt> информацию о рассоглассованности
*в вершинах. В файл <tt>graph.result</tt> записывает колличество рассогласованных
*вершин ( т.е. с отрицательным значением slack ) и номера этих вершин.
*@author Taratuta Yekaterina
*@see reading_file
*/
void filing (void);
/** 
*Эта функция для каждой вершины считает <bb>AAT</bb> - время прихода
*нового сигнала
*@author Taratuta Yekaterina
*@see RAT_count
*/
void AAT_count (void);
/** 
*Эта функция для каждой вершины считает <bb>RAT</bb> - время когда
*сигнал потребуется для вычислений
*@author Taratuta Yekaterina
*@see AAT_count
*/
void RAT_count (void);
/**
*Эта функция для каждой вершины считает <bb>slack</bb> - рассоглассованность в
*вершине. <bb>slack</bb>=<bb>RAT</bb>-<bb>AAT</bb>. 
*@author Taratuta Yekaterina
*@see AAT_count
*@see RAT_count
*/
void slack_count (void);

#endif