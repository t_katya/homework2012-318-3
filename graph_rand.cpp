#include"graph_rand.h"

bool BFS ( int v ) {
	int i, u;
	queue <int> Q;
	Q.push(v);
	color[v] = 0;
	while ( !Q.empty() ) {
		u = Q.front();
		Q.pop();
		if ( color[u] == 1 ) return false;
		for ( i = 0; i < VERTEXES; ++i ) {
			if ( ( graph[u][i] != -1.0 ) && ( graph[u][i] != -2.0 ) && ( graph[u][i] != -3.0 ) && ( color[i] != 0 ) ) {
				Q.push(i);
				color[i] = 0;
			}
		}
		color[u] = 1;
	}
	return true;
}

//---------------------------------------------------------------------------------------

int minimum ( int *mas ) {
	int i, i_min = 0;
	int min = mas[0];
	for ( i = 0; i < VERTEXES; ++i ) {
		if ( min > mas[i] ) {
			min = mas[i];
			i_min = i;
		}
	}
	return i_min;
}

//---------------------------------------------------------------------------------------

int maximum ( int *mas ) {
	int i, i_max = 0;
	int max = mas[0];
	for ( i = 0; i < VERTEXES; ++i ) {
		if ( max < mas[i] ) { 
			max = mas[i];
			i_max = i;
		}
	}
	return i_max;
}

//---------------------------------------------------------------------------------------

double double_randomize( int min, int max ) {
	return min + rand() % ( 1000 * ( max - min ) ) / 1000.0f;
}

//---------------------------------------------------------------------------------------

void randomize_graph ( void ) {
	int i, k, j, t, temp, input = 0, output = 0;
	int number_ways[VERTEXES];
	int way;
	int flg_in[VERTEXES];
	int flg_out[VERTEXES];
	char iseed = ( unsigned int ) time ( NULL );
	srand ( iseed );
	graph = new double *[VERTEXES];
	for ( i = 0; i < VERTEXES; ++i ) { 
		graph[i] = new double [VERTEXES];
		flg_in[i] = 1;
		flg_out[i] = 1;
	}
	while ( input == 0 )
	    input = ( rand() % ( VERTEXES - 1 ) ) / 2;
	while ( output == 0 )
	    output = ( rand () % ( VERTEXES - input ) ) / 2;

	for ( i = 0; i < VERTEXES; ++i ) {
      for ( k = 0; k < VERTEXES; ++k ) graph[i][k] = -1.0;
	}
	for ( i = 0; i < input; ++i ) {
		for ( k = 0; k < VERTEXES; ++k ) 
			graph[k][i] = -2.0;
		flg_in[i] = 0;
	}
	for ( i = VERTEXES - 1; i > VERTEXES - output - 1; --i ) {
		for ( k = 0; k < VERTEXES; ++k ) 
			graph[i][k] = -3.0;
		flg_out[i] = 0;
	}

	for ( i = 0; i < VERTEXES; ++i ) {
		if ( flg_out[i] == 0 ) {
			number_ways[i] = 0;
			continue;
		}
		if ( EDGES == 0 ) number_ways[i] = 0;
		else if ( EDGES == 1 ) number_ways[i] = 1;
		else {
		    number_ways[i] = rand ();
		    if ( number_ways[i] > EDGES ) number_ways[i] = number_ways[i] % EDGES;
			if ( number_ways[i] > VERTEXES - 1 ) number_ways[i] = VERTEXES - 1;
		}
		EDGES = EDGES - number_ways[i];
	}
    while ( EDGES != 0 ) {
	    temp = minimum ( number_ways );
		if ( flg_out[temp] == 1 ) {
	        ++number_ways[temp];
		    --EDGES;
		}
	}
	for ( i = 0; i < VERTEXES; ++i ) {
		if ( ( number_ways[i] == 0 ) && ( flg_out[i] == 1 ) ) {
			temp = maximum ( number_ways );
			number_ways[i] = number_ways[i] + 1;
			number_ways[temp] = number_ways[temp] - 1;
		}
	}
        for ( i = 0; i < VERTEXES; ++i ) {
		    for ( k = 0; k < number_ways[i]; ++k ) {
                way = rand () % VERTEXES;
			    if ( ( way != i ) && ( flg_out[i] == 1 ) && ( ( graph[way][i] == -1.0 ) || ( graph[way][i] == -3.0 ) || 
					( graph[way][i] == -2.0 ) ) && ( flg_in[way] != 0 ) && ( ( graph[i][way] == -1.0 ) || ( graph[i][way] == -3.0 ) || 
					( graph[i][way] == -2.0 ) ) ) {
			        graph[i][way] = double_randomize ( MIN, MAX );
			    }
				else {
					int f = 0;
					for ( j = 0; j < VERTEXES; ++j ) {
						if ( ( flg_out[j] == 0 ) && ( graph[i][j] == -1.0 ) && ( flg_out[i] == 1 ) ) { 
							t = j; 
							f = 1;
							break;
						}
					}
					if ( f == 1 ) graph[i][j] = double_randomize ( MIN, MAX );
					else if ( f == 0 ) {
						for ( j = 0; j < VERTEXES; ++j ) {
						    if ( ( i != j ) && ( ( graph[i][j] == -1.0 ) || ( graph[i][j] == -2.0 ) || ( graph[i][j] == -3.0 ) ) &&
								( flg_in[j] == 1 ) && ( flg_out[i] == 1 ) && ( ( graph[j][i] == -1.0 ) || ( graph[j][i] == -2.0 ) || 
								( graph[j][i] == -3.0 ) ) ) {
								graph[i][j] = double_randomize ( MIN, MAX );  
							    break;
							} 
						}
					}
				}
		    }
	    }
	return;
}

//---------------------------------------------------------------------------------------

void randomize_nodes ( void ) {
	int i;
	char iseed = ( unsigned int ) time ( NULL );
	srand ( iseed );
	nodes = new double [VERTEXES];
	for ( i = 0; i < VERTEXES; ++i ) {
		nodes[i] = double_randomize ( MIN, MAX );
	}
	return;
}

//---------------------------------------------------------------------------------------

void filing ( void ) {
	int i, k;
	fstream f1, f2;
	f1.open( "graph.nodes", fstream::out );
	for ( i = 0; i < VERTEXES; ++i )
	    f1 << nodes[i] << endl;
	f1.close();
	f2.open( "graph.nets", fstream::out );
	    for ( i = 0; i < VERTEXES; ++i ) {
			for ( k = 0; k < VERTEXES; ++k ) {
				if ( ( graph[i][k] != -1.0 ) && ( graph[i][k] != -2.0 ) && ( graph[i][k] != -3.0 ) )
					f2 << i << " " << k << " " << graph[i][k] << endl;
			}
		}
	f2.close();
	return;
}

//---------------------------------------------------------------------------------------

int main () {
	int i;
	color = new int [VERTEXES];
	for ( i = 0; i < VERTEXES; ++i ) 
		color[i] = -1;
	bool t = false;
	while ( t != true ) {
	    randomize_graph ();
		for ( i = 0; i < VERTEXES; ++i ) {
			t = BFS ( i );
			if ( t == false ) break;
		}
	}
	randomize_nodes ();
	filing ();
	//int x; cin>>x;
	return 0;
}