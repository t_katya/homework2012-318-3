#ifndef RANDOMIZE_H
#define RANDOMIZE_H

#include <fstream>
#include <iostream>
#include <ctime>
#include<Queue>

#define VERTEXES 5   
#define MIN 1
#define MAX 10
int EDGES = 6;

using namespace std;

double **graph;
int *inputs;
int *outputs;
double *nodes;
int *color;      //-1 - white, 0 - gray, 1 - black

void filing ( void );
int minimum ( );
bool BFS ( int );
double double_randomize ( int, int );
void randomize_graph ( void );
void randomize_nodes ( void );

#endif