#ifndef HOMEWORK_H
#define HOMEWORK_H

#include<iostream>
#include<fstream>
#include<cstdlib>
#include<cstdio>
#include<Queue>

using namespace std;

int N;
double *ver_delay;
double **graph;
double *AAT;
double *RAT;
int rat_num;
double *slack;

void reading_file (void);
void filing (void);
void AAT_count (void);
void RAT_count (void);
void slack_count (void);

#endif